const express = require('express');
const Track = require('../models/Track');
const router = express.Router();

const createRouter = () => {
    // Track get
    router.get('/', (req, res) => {
        if(req.query.artist) {
            Track.find({artist: req.query.artist})
                .then(tracks => res.send(tracks))
        } else {
            Track.find().populate('artist album')
                .then(tracks => res.send(tracks))
                .catch(() => res.sendStatus(500))
        }
    });

    router.post('/', (req, res) => {
        const trackData = req.body;

        const track = new Track(trackData);
        track.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500))
    });

    return router;
};

module.exports = createRouter;
