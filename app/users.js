const express = require('express');
const User = require('../models/User');
const router = express.Router();
const nanoid = require('nanoid');

const config = require('../config');

const createRouter = () => {

    // User post
    router.post('/', (req, res) => {
        const user = new User(req.body);

        user.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({username: req.body.username});

        if(!user) return res.status(400).send({error: 'User not found'});

        const isMatch = await user.checkPassword(req.body.password);
        if(!isMatch) {
            return res.status(400).send({error: "Incorrect password"});
        }
        user.token = nanoid(10);
        await user.save();
        res.send(user);
    });


    return router;
};

module.exports = createRouter;