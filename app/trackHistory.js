const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const router = express.Router();
const config = require('../config');
const date = new Date();

const createRouter = () => {
    router.post('/', async (req, res) => {
        const token = req.get('token');
        const userToken = await User.findOne({token: token});
        if(!userToken) {
            res.status(401).send({message: 'Unauthorized'});
        } else {
            const trackHistory = new TrackHistory();
            trackHistory.datetime = date.toISOString();
            trackHistory.user = userToken._id;
            trackHistory.track = req.body.track;
            trackHistory.save()
                .then(result => res.send(result))
                .catch(error => res.send(error));
        }
    });

    return router;
};

module.exports = createRouter;