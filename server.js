const express = require('express');
const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const trackHistory = require('./app/trackHistory');
const users = require('./app/users');
const mongoose = require('mongoose');
const config = require('./config');
const app = express();

const port = 8000;

app.use(express.json());
app.use(express.static('public'));

// mongodb://localhost:27017/homework-83
mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
    console.log('Mongoose connected!');

    app.use('/artists', artists());
    app.use('/albums', albums());
    app.use('/tracks', tracks());
    app.use('/users', users());
    app.use('/track_history', trackHistory());

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
